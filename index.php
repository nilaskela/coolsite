<?php

ini_set('error_reporting', E_ALL);
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


require 'libs/Smarty.class.php';
$smarty = new Smarty();

$request = $_GET;
$link=mysqli_connect(
	'localhost',
	'root',
	'password',
	'mysite');

//$smarty->debugging = true;
//$smarty->caching = true;
//$smarty->cache_lifetime = 120;

if (!empty($request['dispath'])) {
	$dispath = $request['dispath'];
} else {
	$dispath = 'index';
}


/*$menu = array(
	'0' => array(
		'name' => 'Main',
		'url' => 'index.php?dispatch=index',
		'active' => $dispath == 'index',
	),
	'1' => array(
		'name' => 'Feedback',
		'url' => 'index.php?dispath=feedback',
		'active' => $dispath == 'feedback',
	),
);
*/
$result=mysqli_query($link, 'set names utf8');
$result= mysqli_query($link, 'select menu_id as id, '
	.'menu_name as name, menu_dispatch as dispath, menu_url as url from menu;'
	);
$menu=array();
if (!$result) {
	echo "error descr...". mysqli_error($link);
	die();
}

if ($result) {
	while ($row = mysqli_fetch_assoc($result)) {
		$id=$row['id'];
		$menu[$id]=$row;
		$menu[$id]['active'] = $row ['dispatch'] == $dispatch;
	}
	# code...
	mysqli_free_result($result);
}


$smarty->assign("menu", $menu);
$smarty->assign("dispatch", $dispatch);

$smarty->display("base.tpl");

?>