                <div class="row">
                    <div class="col-lg-12">
                        <h1 class="page-header">Blank</h1>
                    </div>
                     <div class="row">
                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Kitchen Sink
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                        <tr>
                                     		<td>#</td>
                                            <td>Name</td>
                                            <td>Dispatch</td>
                                            <td>Url</td>
                                        </tr>
                                    </thead>
                                    <tbody>
                                    	{foreach $menu as $item}
                                        <tr>

                                            <td>{$item.id}</td>
                                            <td>{$item.name}</td>
                                            <td>{$item.dispatch}</td>
                                            <td>{$item.url}</td>
                                        </tr>
                                        {/foreach}
                                    </tbody>
                                </table>
                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.panel-body -->
                    </div>
                    <!-- /.panel -->
                </div>
                <!-- /.col-lg-6 -->
                    <!-- /.col-lg-12 -->
                </div>
                <!-- /.row -->
